<?php
/*
 * @Author: Sathish
 * @Desc: Abstract base class to define the seat allocation, departure and arrival points
 */
abstract class Transportation
{
    Protected $seat;
    Protected $transport;
    Protected $departure;
    Protected $arrival;
    Protected $text;

    abstract protected function setSeat($seat);
    abstract protected function getSeat();
    abstract protected function setDeparture($departure);
    abstract protected function getDeparture();
    abstract protected function setArrival($arrival);
    abstract protected function getArrival();   
    abstract protected function setTransport($transport);
    abstract protected function getTransport();   

}

// via bus transport
class bustransport extends Transportation
{
    public function getSeat()
    {
        return $this->seat;
    }

    public function setSeat($seat)
    {
        $this->seat = $seat;
    }

    public function setDeparture($departure)
    {
        $this->departure = $departure;
    }

    public function getDeparture()
    {
        return $this->departure;
    }

    public function setArrival($arrival)
    {
        $this->arrival= $arrival;
    }

    public function getArrival()
    {
        return $this->arrival;
    }

    public function getTransport()
    {
      return $this->transport;
    }

    public function setTransport($transport)
    {
      $this->transport = $transport;
    }

    public function __toString()
    {
        return " Take ".$this->transport." from ".$this->departure." to ".$this->arrival. ", Seat ". $this->seat;
    }

}

// via air transport
class airtransport extends Transportation
{
    public function getSeat()
    {
        return $this->seat;
    }

    public function setSeat($seat)
    {
        $this->seat = $seat;
    }

    public function setDeparture($departure)
    {
        $this->departure = $departure;
    }

    public function getDeparture()
    {
        return $this->departure;
    }

    public function setArrival($arrival)
    {
        $this->arrival= $arrival;
    }

    public function getArrival()
    {
        return $this->arrival;
    }

    public function getTransport()
    {
      return $this->transport;
    }

    public function setTransport($transport)
    {
      $this->transport = $transport;
    }

    public function getText()
    {
       return $this->text;
    }

    public function setText($text)
    {
       $this->text = $text;
    }

    public function __toString()
    {
        return " Take ".$this->transport." from ".$this->departure." to ".$this->arrival. ", Seat ". $this->seat .", ". $this->text;
    }
}

// via train transport
class traintransport extends Transportation
{
    public function getSeat()
    {
        return $this->seat;
    }

    public function setSeat($seat)
    {
        $this->seat = $seat;
    }

    public function setDeparture($departure)
    {
        $this->departure = $departure;
    }

    public function getDeparture()
    {
        return $this->departure;
    }

    public function setArrival($arrival)
    {
        $this->arrival= $arrival;
    }

    public function getArrival()
    {
        return $this->arrival;
    }

    public function getTransport()
    {
      return $this->transport;
    }

    public function setTransport($transport)
    {
      $this->transport = $transport;
    }

    public function __toString()
    {
        return " Take ".$this->transport." from ".$this->departure." to ".$this->arrival. ", Seat ". $this->seat;
    }
}

// via road transport  class extendable for road etc
class roadtransport extends Transportation
{
   public function getSeat()
   {
       return $this->seat;
   }

   public function setSeat($seat)
   {
       $this->seat = $seat;
   }

   public function setDeparture($departure)
   {
       $this->departure = $departure;
   }

   public function getDeparture()
   {
       return $this->departure;
   }

   public function setArrival($arrival)
   {
       $this->arrival= $arrival;
   }

   public function getArrival()
   {
       return $this->arrival;
   }

    public function getTransport()
    {
      return $this->transport;
    }

    public function setTransport($transport)
    {
      $this->transport = $transport;
    }

   public function __toString()
   {
       return " Take ".$this->transport." from ".$this->departure." to ".$this->arrival. ", Seat ". $this->seat;

   }
}
?>