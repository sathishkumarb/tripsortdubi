# tripsortdubi

This API project to order the trips

# installation

Download and extract the zip file

# install composer globally and phpunit with vendor directory

a) composer update

b) composer require phpunit ^7

# directory structure

class/transportation/
data/
tests/
tests/data
vendor/
.itignore
composer.json

# 1) Run the app from browser

	# Use the below lines in your index.php

	require_once 'class/boardingCards.php';
	require_once 'class/Transportation.php';

	$transport = new sortboardingCards($type);

	$transport->listboardingCards();


	http://localhost/[folder]/index.php?type=[type]
	
	Ex:
	Folder -> installed folder
	type -> please refer data directory filenames (sorted,unsorted)

	#sample results

	please refer attached screenshots

#2) Run the app from command line in doc root

	php index.php data/[filename]
	
	Ex:
	filename -> please refer data directory filenames (sorted,unsorted)

#3) Implmenation of o'notation linear complexity but two times

# foreach ($this->cards as $value) --> o(n)

# foreach ($this->cards as $index => $path)  --> o(n)

# can check the browser execute test time of the script for different cards

#3) Run phpunit test cases 

	#a) phpunit tests/TransportationTest [options]
	

	1) To trial out change the TransportationTest class filenames in the test cases in the setUp() method as pointed in step 2.


	2) Where to look

	Refer  the directory 'tests/data/'

	a) sorted.json
	b) unsorted.json
	c) broken.json 
	d) nothing.txt ->filematch check -->change valid file
	e) nothing.json

	3) I have made some error tests and failed tests and can be mocked around to pass

	Sample Output ::

	ERRORS!
	Tests: 16, Assertions: 16, Errors: 3, Failures: 3.